#version 330

in vec3 eyeNormal;
in vec3 lightDirection;
in vec2 texCoords;
in vec4 regColor;

uniform vec4 diffuseColor;
uniform vec4 ambientColor;
uniform vec4 specularColor;

uniform sampler2D texMap;

uniform bool useTex;

out vec4 fColor;

void main(void)
{
  if (!useTex)
    fColor = regColor;
  else
  {
    vec4 texSample = texture(texMap, texCoords);
///     if (texSample.r < 0.3)
///       discard;

    float diffuse = max(0.0f, dot(eyeNormal, lightDirection));

    fColor = diffuse * diffuseColor;
    fColor += ambientColor;

    fColor *= texSample;

    vec3 reflection = normalize(reflect(-lightDirection, eyeNormal));
    float specular = max(0.0, dot(eyeNormal, reflection));

    if (diffuse != 0)
    {
      float spec = pow(specular, 128.0f);
      fColor.rgb += vec3(spec, spec, spec) * specularColor.rgb;
    }
  }
}