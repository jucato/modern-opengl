#ifndef SHADERS_H
#define SHADERS_H

#include <GL/glew.h>

#include <iostream>
#include <fstream>
#include <string>

// Shaders
GLuint vertexShader, fragmentShader, shaderProgram;

std::string loadShaderFile(const std::string& fileName)
{
  std::ifstream file;
  file.open((fileName).c_str());

  std::string output;
  std::string line;

  if(file.is_open())
  {
    while(file.good())
    {
      std::getline(file, line);
      output.append(line + "\n");
    }
  }
  else
  {
    std::cerr << "Unable to load shader file: " << fileName << std::endl;
  }

  return output;
}

bool initializeShaders(void)
{
  std::string vertexShaderFile, fragmentShaderFile;
  GLchar* shaderString[1];
  GLint status;

  // setup vertex shader
  vertexShaderFile = loadShaderFile("modern.vs");
  vertexShader = glCreateShader(GL_VERTEX_SHADER);
  shaderString[0] = (GLchar*) vertexShaderFile.c_str();
  glShaderSource(vertexShader, 1, (const GLchar**) shaderString, NULL);
  glCompileShader(vertexShader);
  glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &status);

  if (status == GL_FALSE)
  {
    char infoLog[1024];
    glGetShaderInfoLog(vertexShader, 1024, NULL, infoLog);
    std::cerr << "Error compiling vertex shader: " << infoLog << std::endl;
    glDeleteShader(vertexShader);

    return false;
  }

  // setup fragment shader
  fragmentShaderFile = loadShaderFile("modern.fs");
  fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  shaderString[0] = (GLchar*) fragmentShaderFile.c_str();
  glShaderSource(fragmentShader, 1, (const GLchar**) shaderString, NULL);
  glCompileShader(fragmentShader);
  glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &status);

  if (status == GL_FALSE)
  {
    char infoLog[1024];
    glGetShaderInfoLog(fragmentShader, 1024, NULL, infoLog);
    std::cerr << "Error compiling fragment shader: " << infoLog << std::endl;
    glDeleteShader(fragmentShader);

    return false;
  }

  // Create final shader program
  shaderProgram = glCreateProgram();

  glAttachShader(shaderProgram, vertexShader);
  glAttachShader(shaderProgram, fragmentShader);

  glBindAttribLocation(shaderProgram, 0, "vPos");
  glBindAttribLocation(shaderProgram, 1, "vCol");
  glBindAttribLocation(shaderProgram, 2, "vNorm");
  glBindAttribLocation(shaderProgram, 3, "vTex");

  glLinkProgram(shaderProgram);
  glGetProgramiv(shaderProgram, GL_LINK_STATUS, &status);

  if (status == GL_FALSE)
  {
    char infoLog[1024];
    glGetProgramInfoLog(shaderProgram, 1024, NULL, infoLog);
    std::cerr << "Error linking shader program: " << infoLog << std::endl;
    glDeleteProgram(shaderProgram);

    return false;
  }

  return true;
}

GLint getUniform(const GLchar* name)
{
  GLint uniform = glGetUniformLocation(shaderProgram, name);

  if (uniform == -1)
  {
    std::cerr << "Could not find uniform " << name << std::endl;
    return 0;
  }

  return uniform;
}

void useShader(void)
{
  glUseProgram(shaderProgram);
}

void destroyShaders(void)
{
  glDetachShader(shaderProgram, vertexShader);
  glDetachShader(shaderProgram, fragmentShader);
  glDeleteShader(vertexShader);
  glDeleteShader(fragmentShader);
  glDeleteProgram(shaderProgram);
}

#endif