#version 330

in vec4 vPos;
in vec4 vCol;
in vec3 vNorm;
in vec2 vTex;

uniform mat4 mvp;
uniform mat4 modelview;
uniform mat3 normalMatrix;
uniform vec3 lightPos;

out vec3 eyeNormal;
out vec3 lightDirection;
out vec2 texCoords;
out vec4 regColor;

void main(void)
{
  eyeNormal = normalize(normalMatrix * vNorm);

  vec4 vecPos4 = modelview * vPos;
  vec3 vecPos3 = vecPos4.xyz / vecPos4.w;

  lightDirection = normalize(lightPos - vecPos3);

  gl_Position = mvp * vPos;
  texCoords = vTex;
  regColor = vCol;
}