#include "SDLGLApp.h"
#include "Shaders.h"
#include "Mesh.h"

#include <iostream>

void SDLGLApp::init(std::string windowTitle, int width, int height)
{
  if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    errorExit("SDL initialization failed", SDL_GetError());
  
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3); 
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0); 
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

  SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1); 

  m_window = SDL_CreateWindow(windowTitle.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL);
  if (!m_window)
    errorExit("SDL create window failed: ", SDL_GetError());
  
  m_glContext = SDL_GL_CreateContext(m_window);
  if (!m_glContext)
    errorExit("OpenGL context creation failed :", SDL_GetError());
  
  GLenum err = glewInit();
  if (err != GLEW_OK)
    errorExit("GLEW init failed: ", (char*) glewGetErrorString(err));

  glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
  
  initializeShaders();
  initializeVertices();
  
  m_isRunning = true;
}

void SDLGLApp::handleEvents(void)
{
  while (SDL_PollEvent(m_event))
  {
    if (m_event->type == SDL_QUIT)
      m_isRunning = false;
  }
}

void SDLGLApp::update(void)
{

}

void SDLGLApp::draw(void)
{
  glClear(GL_COLOR_BUFFER_BIT);
  
  SDL_GL_SwapWindow(m_window);
}

void SDLGLApp::cleanup(void)
{

}

void SDLGLApp::errorExit(std::string customMsg, std::string errorMsg)
{
  std::cout << customMsg << errorMsg << std::endl;
  
  this->cleanup();
  SDL_Quit();
  exit(1);
}
