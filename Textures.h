#ifndef TEXTURES_H
#define TEXTURES_H

#ifdef WIN32
  #define GLEW_STATIC
#endif
#include <GL/glew.h>

#include <SDL2/SDL_image.h>

#include <iostream>

GLuint loadTexture(const char* filename)
{
  int width, height, numComponents;

  SDL_Surface* imageData = IMG_Load(filename);

  if (imageData == NULL)
  {
    std::cerr << "Failed loading texture " << filename << std::endl;
    return -1;
  }

  GLenum mode;
  if (imageData->format->BytesPerPixel == 3)
    mode = GL_RGB;
  else if (imageData->format->BytesPerPixel == 3)
    mode = GL_RGBA;
  else
    return -1;

  GLuint texture;

  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

  glTexImage2D(GL_TEXTURE_2D, 0, mode, imageData->w, imageData->h, 0, mode, GL_UNSIGNED_BYTE, imageData->pixels);

  SDL_FreeSurface(imageData);

  return texture;
}

void useTexture(GLuint texID)
{
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, texID);
}

void destroyTextures(GLuint texID)
{
  glDeleteTextures(1, &texID);
}

#endif
