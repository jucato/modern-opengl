#ifndef SDLGDLAPP_H
#define SDLGDLAPP_H

#include <SDL2/SDL.h>
#include <GL/glew.h>

#include <string>

class SDLGLApp 
{
  public:
    SDLGLApp() : m_isRunning(false) {}
    ~SDLGLApp() {}
    
    void init(std::string windowTitle, int width, int height);
    void handleEvents(void);
    void update(void);
    void draw(void);
    void cleanup(void);

    bool isRunning(void) { return m_isRunning; }

  private:
    void errorExit(std::string customMsg, std::string errorMsg = "");
    
    SDL_Window* m_window;
    SDL_GLContext m_glContext;
    SDL_Event* m_event;

    bool m_isRunning;
};

#endif