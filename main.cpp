#include <SDL2/SDL.h>
#ifdef WIN32
  #define GLEW_STATIC
#endif
#include <GL/glew.h>
#include <iostream>

#include "Shaders.h"
#include "Mesh.h"
#include "Math.h"
#include "Textures.h"

SDL_Window* window;
SDL_GLContext glContext;
SDL_Event event;
bool isRunning = false;

const int width = 800;
const int height = 600;
mat4 mvp, projection, modelview, model, view;

float angle = 0.0f;
vec3 axis = { 0.0f, 0.0f, 0.0f };
vec3 pos = { 0.0f, 0.0f, 0.0f };
float xRot = 0.0f, yRot = 1.0f , zRot = 1.0f;
mat4 anim, tempModel;
bool isRotate = true;

vec3 eye = { 5.0f, 5.0f, 5.0f };
vec3 at = { 0.0f, 0.0f, 0.0f };
vec3 up = { 0.0f, 1.0f, 0.0f };
float cameraAngle = 0.0f;

const int FPS = 60;
const int DELAY = 1000 /  FPS;

const vec4 diffuseColor = { 0.0f, 0.0f, 1.0f, 1.0f };
const vec4 ambientColor = { 0.5f, 0.5f, 0.5f, 1.0f };
const vec4 specularColor = { 1.0f, 1.0f, 1.0f, 1.0f };

const vec3 lightPos = { -100.0f, 100.0f, -100.0f };
mat3 normalMatrix;

void getRotMatrix(mat3 rotMatrix, mat4 m)
{
  rotMatrix[0] = m[0];            rotMatrix[3] = m[4];              rotMatrix[6] = m[8];
  rotMatrix[1] = m[1];            rotMatrix[4] = m[5];              rotMatrix[7] = m[9];
  rotMatrix[2] = m[2];            rotMatrix[5] = m[6];              rotMatrix[8] = m[10];
}

GLuint texID;

bool init(const char* windowName, int width, int height);
void handleEvents(void);
void update(void);
void draw(void);
void cleanup(void);

int main(int argc, char* argv[])
{
  unsigned int frameStart, frameTime;

  cameraAngle = atanf(eye[2] / eye[0]);

  if (!init("SDL GL Window", width, height))
  {
    cleanup();
    exit(1);
  }

  while (isRunning)
  {
    frameStart = SDL_GetTicks();

    handleEvents();
    update();
    draw();

    frameTime = SDL_GetTicks() - frameStart;
    if (frameTime < DELAY)
      SDL_Delay(DELAY - frameTime);
  }

  cleanup();

  return 0;
}

bool init(const char* windowName, int width, int height)
{
  // Initialize SDL
  if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
  {
    std::cerr << "SDL init failed: " << SDL_GetError() << std::endl;
    return false;
  }

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

  SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

  window = SDL_CreateWindow(windowName, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL);
  if (!window)
  {
    std::cerr << "SDL create window failed: " << SDL_GetError() << std::endl;
    return false;
  }

  SDL_GLContext glContext = SDL_GL_CreateContext(window);
  if (!glContext)
  {
    std::cerr << "OpenGL context creation failed :" << SDL_GetError() << std::endl;
    return false;
  }

  glewExperimental = true;
  GLenum err = glewInit();
  if (GLEW_OK != err)
  {
    std::cerr << "GLEW init failed: " << glewGetErrorString(err) << std::endl;
    return false;
  }

  glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
  glEnable(GL_DEPTH_TEST);
  glViewport(0, 0, width, height);

  matrixIdentity(model);
  matrixIdentity(view);
  matrixIdentity(modelview);
  matrixIdentity(mvp);

  matrixIdentity(projection);
  matrixPerspective(projection, 45.0f, float(width)/float(height), 0.1f, 100.0f);

  initializeShaders();

  initializeVertices();

  texID = loadTexture("bricks.jpg");

  isRunning = true;

  return true;
}

void handleEvents(void)
{
  vec3 tempVec = {eye[0], 0.0f, eye[2]};
  float radius;

  while (SDL_PollEvent(&event))
  {
    switch (event.type)
    {
      case SDL_QUIT:
        isRunning = false;
        break;
      case SDL_KEYDOWN:
        int key = event.key.keysym.sym;

        if ((key == SDLK_ESCAPE) || (key == SDLK_q))
          isRunning = false;
        if (key == SDLK_SPACE)
          isRotate = !isRotate;
        if (key == SDLK_x)
          xRot = (xRot == 1.0f) ? 0.0f : 1.0f;
        if (key == SDLK_y)
          yRot = (yRot == 1.0f) ? 0.0f : 1.0f;
        if (key == SDLK_z)
          zRot = (zRot == 1.0f) ? 0.0f : 1.0f;
        if (key == SDLK_UP)
          pos[1] += 0.1f;
        if (key == SDLK_DOWN)
          pos[1] -= 0.1f;
        if (key == SDLK_LEFT)
          pos[0] -= 0.1f;
        if (key == SDLK_RIGHT)
          pos[0] += 0.1f;
        if (key == SDLK_PERIOD)
          pos[2] -= 0.1f;
        if (key == SDLK_COMMA)
          pos[2] += 0.1f;
        if (key == SDLK_a)
        {
          cameraAngle += deg_to_rad(1);
          radius = vectorMagnitude(tempVec);
          eye[0] = cosf(cameraAngle) * radius;
          eye[2] = sinf(cameraAngle) * radius;
        }
        if (key == SDLK_d)
        {
          cameraAngle -= deg_to_rad(1);
          radius = vectorMagnitude(tempVec);
          eye[0] = cosf(cameraAngle) * radius;
          eye[2] = sinf(cameraAngle) * radius;
        }
        if (key == SDLK_w)
        {
          radius = vectorMagnitude(tempVec) - 0.1;
          eye[0] = cosf(cameraAngle) * radius;
          eye[2] = sinf(cameraAngle) * radius;
        }
        if (key == SDLK_s)
        {
          radius = vectorMagnitude(tempVec) + 0.1;
          eye[0] = cosf(cameraAngle) * radius;
          eye[2] = sinf(cameraAngle) * radius;
        }
        if (key == SDLK_o)
            matrixOrtho(projection, -10.0f, 10.0f, -10.0f, 10.0f, -20.0f, 20.0f);
        if (key == SDLK_p)
          matrixPerspective(projection, 45.0f, float(width)/float(height), 0.1f, 100.0f);
        if (key == SDLK_r)
        {
          angle = 0.0f;
          cameraAngle = deg_to_rad(45);
          eye[0] = eye[1] = 0; eye[2] = 5.0f;
          pos[0] = pos[1] = pos[2] = 0;
          matrixPerspective(projection, 45.0f, float(width)/float(height), 0.1f, 100.0f);
        }
        break;
    }
  }
}

void update(void)
{
  if (isRotate)
    angle += 1.0f;

  axis[0] = xRot;
  axis[1] = yRot;
  axis[2] = zRot;
}

void draw(void)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  useShader();

  GLint mvpUniform = getUniform("mvp");
  GLint mvUniform = getUniform("modelview");
  GLint normUniform = getUniform("normalMatrix");
  GLint lightPosUniform = getUniform("lightPos");

  GLint diffuseUniform = getUniform("diffuseColor");
  GLint ambientUniform = getUniform("ambientColor");
  GLint specUniform = getUniform("specularColor");

  GLint texUniform = getUniform("texMap");

  GLint useTexUniform = getUniform("useTex");

  matrixIdentity(model);
  matrixLookAt(view, eye, at, up);
  matrixMult(modelview, view, model);
  matrixMult(mvp, projection, modelview);

  glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, mvp);
  glUniform1i(useTexUniform, 0);
  drawAxes();

  glUniform1i(useTexUniform, 1);
  glUniform1i(texUniform, 0);
  drawGrid();

  matrixIdentity(anim);
  if (vectorMagnitude(axis))
    matrixRotate(anim, angle, axis);
  matrixTranslate(tempModel, pos);
  matrixMult(model, tempModel, anim);

  matrixLookAt(view, eye, at, up);
  matrixMult(modelview, view, model);

  matrixMult(mvp, projection, modelview);

  glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, mvp);
  glUniformMatrix4fv(mvUniform, 1, GL_FALSE, modelview);

  getRotMatrix(normalMatrix, modelview);
  glUniformMatrix3fv(normUniform, 1, GL_FALSE, normalMatrix);

  glUniform4fv(diffuseUniform, 1, diffuseColor);
  glUniform4fv(ambientUniform, 1, ambientColor);
  glUniform4fv(specUniform, 1, specularColor);
  glUniform3fv(lightPosUniform, 1, lightPos);

  glUniform1i(useTexUniform, 1);
  glUniform1i(texUniform, 0);
  useTexture(texID);

  drawVertices();

  SDL_GL_SwapWindow(window);
}

void cleanup(void)
{
    destroyShaders();

    destroyVertices();

    destroyTextures(texID);

    if (glContext)
        SDL_GL_DeleteContext(glContext);

    if (window)
        SDL_DestroyWindow(window);

    SDL_Quit();
}
