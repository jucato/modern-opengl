#ifndef MATH_H
#define MATH_H

#include <iostream>
#include <iomanip>
#include <cstring>
#include <cmath>

#define PI 3.14159265358979323846264380f
#define deg_to_rad(x) (x * PI / 180)
#define rad_to_deg(x) (x * 180 / PI)

typedef float vec2[2];
typedef float vec3[3];
typedef float vec4[4];

typedef float mat4[16];
/* 0  4  8  12
   1  5  9  13
   2  6  10 14
   3  7  11 15
*/

typedef float mat3[9];
/* 0  3  6
   1  4  7
   2  5  8
*/

/**************/

/*** VERTICES ***/

/*
	Dot Product - V1 . V2
	a	x		ax + by + cz + dw
	b	y
	c	z
	d	w
*/

void vectorPrint(vec3 vec)
{
  std::cout << std::fixed << std::setprecision(3);
  std::cout << "[" << vec[0] << ", " << vec[1] << ", " << vec[2] << "]" << std::endl;
}

float vectorMagnitude(vec3 vec)
{
    float len = (vec[0] * vec[0]) + (vec[1] * vec[1]) + (vec[2] * vec[2]);

    if (len == 0)
      return 0;
    else
      return (float) sqrt(len);
}

float vectorDot(vec3 v1, vec3 v2)
{
  return ((v1[0] * v2[0]) + (v1[1] * v2[1]) + (v1[2] * v2[2]));
}

/*
	Cross Product - V1 x V2
	a	A	(bC - cB), (cA - aC), (aB - bA), 0
	b	B
	c	C
	0	0
*/

void vectorCross(vec3 res, vec3 v1, vec3 v2)
{
  vec3 temp;

  temp[0] = (v1[1] * v2[2]) - (v1[2] * v2[1]);
  temp[1] = (v1[2] * v2[0]) - (v1[0] * v2[2]);
  temp[2] = (v1[0] * v2[1]) - (v1[1] * v2[0]);

  memcpy(res, temp, sizeof(vec3));
}

void vectorNormalize(vec3 vec)
{
  float magnitude = vectorMagnitude(vec);

  vec[0] /= magnitude;
  vec[1] /= magnitude;
  vec[2] /= magnitude;
}

void vectorScalarMul(vec3 res, vec3 v, float s){
  vec3 temp;

  temp[0]  = v[0] * s;
  temp[1]  = v[1] * s;
  temp[2]  = v[2] * s;

  memcpy(res, temp, sizeof(vec3));
}

void vectorAdd(vec3 res, vec3 a, vec3 b)
{
  vec3 temp;

  temp[0] = a[0] + b[0];
  temp[1] = a[1] + b[1];
  temp[2] = a[2] + b[2];

  memcpy(res, temp, sizeof(vec3));
}

void vectorSub(vec3 res, vec3 a, vec3 b)
{
  vec3 temp;

  temp[0] = a[0] - b[0];
  temp[1] = a[1] - b[1];
  temp[2] = a[2] - b[2];

  memcpy(res, temp, sizeof(vec3));
}


/**************/

/*** MATRICES ***/

/*
	MxV - Multiply matrix M by vector V
	a b c d		x		ax + by + cz + dw
	e f g h		y		ex + fy + gz + hw
	i j k l		z		ix + jy + kz + lw
	m n o p		w		mx + ny + oz + pw
*/

void matrixPrint(mat4 mat)
{
  std::cout << std::fixed << std::setprecision(3);
  std::cout << "[ " << mat[0] << "\t" << mat[4] << "\t" << mat[8]  << "\t" << mat[12] << " ]" << std::endl;
  std::cout << "[ " << mat[1] << "\t" << mat[5] << "\t" << mat[9]  << "\t" << mat[13] << " ]" << std::endl;
  std::cout << "[ " << mat[2] << "\t" << mat[6] << "\t" << mat[10] << "\t" << mat[14] << " ]" << std::endl;
  std::cout << "[ " << mat[3] << "\t" << mat[7] << "\t" << mat[11] << "\t" << mat[15] << " ]" << std::endl;
}

void matrixVectorMult4(vec4 res, mat4 mat, vec4 vec)
{
  vec4 temp;
  memset(temp, 0, sizeof(vec4));

  temp[0] = (mat[0] * vec[0]) + (mat[4] * vec[1]) + (mat[8]  * vec[2]) + (mat[12] * vec[3]);
  temp[1] = (mat[1] * vec[0]) + (mat[5] * vec[1]) + (mat[9]  * vec[2]) + (mat[13] * vec[3]);
  temp[2] = (mat[2] * vec[0]) + (mat[6] * vec[1]) + (mat[10] * vec[2]) + (mat[14] * vec[3]);
  temp[3] = (mat[3] * vec[0]) + (mat[7] * vec[1]) + (mat[11] * vec[2]) + (mat[15] * vec[3]);

  memcpy(res, temp, sizeof(vec4));
}

void matrixVectorMult3(vec3 res, mat4 mat, vec3 vec)
{
  vec3 temp;
  memset(temp, 0, sizeof(vec3));

  res[0] = (mat[0] * vec[0]) + (mat[4] * vec[1]) + (mat[8]  * vec[2]);
  res[1] = (mat[1] * vec[0]) + (mat[5] * vec[1]) + (mat[9]  * vec[2]);
  res[2] = (mat[2] * vec[0]) + (mat[6] * vec[1]) + (mat[10] * vec[2]);

  float w = (mat[3] * vec[0]) + (mat[7] * vec[1]) + (mat[11] * vec[2]);
  if ((w != 0) && (w != 1))
  {
    res[0] /= w;
    res[1] /= w;
    res[2] /= w;
  }

  memcpy(res, temp, sizeof(vec3));
}

void matrixScalarMult(mat4 res, mat4 m, float scalar)
{
  mat4 temp;
  memset(temp, 0, sizeof(mat4));

  for (int r = 0; r < 4; r++)
    for (int c = 0; c < 4; c++)
      temp[(c << 2) + r] = m[(c << 2) + r] * scalar;

  memcpy(res, temp, sizeof(mat4));
}

void matrixAdd(mat4 res, mat4 a, mat4 b)
{
  mat4 temp;
  memset(temp, 0, sizeof(mat4));

  for (int r = 0; r < 4; r++)
    for (int c = 0; c < 4; c++)
      temp[(c << 2) + r] = a[(c << 2) + r] + b[(c << 2) + r];

  memcpy(res, temp, sizeof(mat4));
}

void matrixSub(mat4 res, mat4 a, mat4 b)
{
  mat4 temp;
  memset(temp, 0, sizeof(mat4));

  for (int r = 0; r < 4; r++)
    for (int c = 0; c < 4; c++)
      res[(c << 2) + r] = a[(c << 2) + r] - b[(c << 2) + r];

  memcpy(res, temp, sizeof(mat4));
}

/*
      MxM -= Multiply matrix M by matrix M
      a b c d	A B C D		(aA + bE + cI + dM)	(aB + bF + cJ + dN)	(aC + bG + cK + dO)	(aD + bH + cL + dP)
      e f g h	E F G H		(eA + fB + gI + hM)	(eB + fF + gJ + hN)	(eC + fG + gK + hO)	(eD + fH + gL + hP)
      i j k l	I J K L		(iA + jE + kI + lM)	(iB + jF + kJ + lN)	(iC + jG + kK + lO)	(iD + jH + kL + lP)
      m n o p	M N O P		(mA + nE + oI + pM)	(mB + nF + oJ + pN)	(mC + nG + oK + pO)	(mD + nH + oL + pP)

      M[row, col] = a[row, 0] * b[0, col] + a[row, 1] * b[1, col] + a[row, 2] * b[2, col] + a[row, 3] * b[3, col]
*/

void matrixMult(mat4 res, mat4 a, mat4 b)
{
  mat4 temp;
  memset(temp, 0, sizeof(mat4));

//   for (int row = 0; row < 4; row++)
//     for (int col = 0; col < 4; col++)
//       for (int e = 0; e < 4; e++)
// 	temp[(col << 2) + row] += a[(e << 2) + row] * b[(col << 2) + e];

/* Alternate implementation */
  for (int row = 0; row < 4; row++)
    for (int col = 0; col < 4; col++)
	temp[(col << 2) + row] = (a[(0 << 2) + row] * b[(col << 2) + 0]) +
                                 (a[(1 << 2) + row] * b[(col << 2) + 1]) +
                                 (a[(2 << 2) + row] * b[(col << 2) + 2]) +
                                 (a[(3 << 2) + row] * b[(col << 2) + 3]);

  memcpy(res, temp, sizeof(mat4));
}

void matrixIdentity(mat4 m)
{
  memset(m, 0, sizeof(mat4));

  for (int i = 0; i < 4; i++)
    m[(i << 2) + i] = 1.0f;
}

/*
    Translation Matrix
    translate by (x, y, z)
    1 0 0 x
    0 1 0 y
    0 0 1 z
    0 0 0 1
*/

void matrixTranslate(mat4 m, vec3 v)
{
  matrixIdentity(m);

  m[12] = v[0];
  m[13] = v[1];
  m[14] = v[2];
}

/*
    Scaling Matrix
    scale by (x, y, z)
    x 0 0 0
    0 y 0 0
    0 0 z 0
    0 0 0 1
*/

void matrixScale(mat4 m, vec3 v)
{
  matrixIdentity(m);

  m[0]  = v[0];
  m[5]  = v[1];
  m[10] = v[2];
}

/*
  Rotation Formulas:

  rotateX:  1.0       0.0       0.0       0.0
            0.0     cos(a)    -sin(a)     0.0
            0.0     sin(a)     cos(a)     0.0
            0.0       0.0       0.0       1.0

  rotateY: cos(a)     0.0      sin(a)     0.0
            0.0       1.0       0.0       0.0
          -sin(a)     0.0      cos(a)     0.0
            0.0       0.0       0.0       1.0

  rotateZ: cos(a)   -sin(a)     0.0       0.0
           sin(a)    cos(a)     0.0       0.0
            0.0       0.0       1.0       1.0
            0.0       0.0       0.0       1.0

    m[0] * cos(a)     m[1] * -sin(a)    m[2] * sin(a)     m[3] = 0
    m[4] * sin(a)     m[5] * cos(a)     m[6] * -sin(a)    m[7] = 0
    m[8] * -sin(a)    m[9] * sin(a)     m[10] * cos(a)    m[11] = 0
    m[12] = 0         m[13] = 0         m[141] = 0        m[15] = 1
*/

void matrixRotateX(mat4 m, float angle)
{
  matrixIdentity(m);

  float angle_in_rad = deg_to_rad(angle);
  float s = sinf(angle_in_rad);
  float c = cosf(angle_in_rad);

  m[5] =  c;   // m[1, 1]
  m[6] =  s;   // m[2, 1]
  m[9] = -s;   // m[1, 2]
  m[10] = c;   // m[2, 2]
}

void matrixRotateY(mat4 m, float angle)
{
  matrixIdentity(m);

  float angle_in_rad = deg_to_rad(angle);
  float s = sinf(angle_in_rad);
  float c = cosf(angle_in_rad);

  m[0] =  c;   // m[0, 0]
  m[2] = -s;   // m[2, 0]
  m[8] =  s;   // m[0, 2]
  m[10] = c;   // m[2, 2]
}

void matrixRotateZ(mat4 m, float angle)
{
  matrixIdentity(m);

  float angle_in_rad = deg_to_rad(angle);
  float s = sinf(angle_in_rad);
  float c = cosf(angle_in_rad);

  m[0] =  c;   // m[0, 0]
  m[1] =  s;   // m[1, 0]
  m[4] = -s;   // m[0, 1]
  m[5] =  c;   // m[1, 1]
}

/*
  Rotate by arbitrary axis

  x, y, z = normalize(x, y z)
  a = angle in radians

  (x * x * (1 - cos(a))) + cos(a)         (y * x * (1 - cos(a))) + (z * sin(a))         (z * x * (1 - cos(a))) - (y * sin(a))         0
  (x * y * (1 - cos(a))) - (z * sin(a))   (y * y * (1 - cos(a))) + cos(a)               (z * y * (1 - cos(a))) + (x * sin(a))         0
  (x * z * (1 - cos(a))) + (y * sin(a))   (y * z * (1 - cos(a))) - (x * sin(a))         (z * z * (1 - cos(a))) + cos(a)               0
  0                                       0                                             0                                             1

*/

void matrixRotate(mat4 m, float angle, vec3 v)
{
  vec3 n;
  vectorNormalize(v);

  float angle_in_rad = deg_to_rad(angle);
  float c = cosf(angle_in_rad);
  float s = sinf(angle_in_rad);
  float c1 = 1 - c;

  float x = v[0];
  float y = v[1];
  float z = v[2];

  matrixIdentity(m);

  m[0] = (x * x * c1) + c;		m[4] = (x * y * c1) - (z * s);		m[8] = (x * z * c1) + (y * s);		// m[12] = 0;
  m[1] = (y * x * c1) + (z * s);	m[5] = (y * y * c1) + c;		m[9] = (y * z * c1) - (x * s);		// m[13] = 0;
  m[2] = (z * x * c1) - (y * s);	m[6] = (z * y * c1) + (x * s);		m[10] = (z * z * c1) + c;		// m[14] = 0;
  // m[3] = 0;				m[7] = 0;				m[11] = 0;				// m[15] = 1;
}

/*
    Create Orthogonal projection matrix

    2 / (right - left)                0                       0               - ((right + left) / (right - left))
    0                           2 / (top - bottom)            0               - ((top + bottom) / (top - bottom))
    0                                 0               -(2 / (far - near))     - ((far + near) / (far - near))
    0                                 0                       0                         1
*/
void matrixOrtho(mat4 m, float left, float right, float bottom, float top, float near, float far)
{
  matrixIdentity(m);

  float rl = right - left;
  float tb = top - bottom;
  float fn = far - near;

  m[0] = 2 / rl;                m[4] = 0;               m[8] = 0;               m[12] = -((right + left) / rl);
  m[1] = 0;                     m[5] = 2 / tb;          m[9] = 0;               m[13] = -((top + bottom) / tb);
  m[2] = 0;                     m[6] = 0;               m[10] = -(2 / fn);      m[14] = -((far + near) / fn);
  // m[3] = 0;                  m[7] = 0;               m[11] = 0;              m[15] = 1;
}

/*
    Create Perspective projection matrix

    fov = vertical field of view in radians (fovy / 180 * pi)
    f = 1 / tan(fov / 2)
    aspect = aspect ratio (width / height)

    f / aspect          0               0                       0
    0                   f               0                       0
    0                   0       -(far + near / near - far)  -(2 * far * near / near - far)
    0                   0              -1                       0
*/

void matrixPerspective(mat4 m, float fovy, float aspect, float near, float far)
{
  matrixIdentity(m);

  float f = 1 / tanf(fovy / 2);
  float fn = far - near;

  m[0] = f / aspect;            m[4] = 0;               m[8] = 0;                       m[12] = 0;
  m[1] = 0;                     m[5] = f;               m[9] = 0;                       m[13] = 0;
  m[2] = 0;                     m[6] = 0;               m[10] = -((far + near) /fn);    m[14] = -((2 * far * near) / fn);
  m[3] = 0;                     m[7] = 0;               m[11] = -1;                     m[15] = 0;
}

/*
    lookAt - convenience function to simulate a camera, like old-style gluLookAt

    reference: https://github.com/arkanis/single-header-file-c-libs/blob/master/math_3d.h

    lookAt(camera position, camera target, camera up direction)

    forward - distance between target and position
    "left" x-axis - vector orthogonal to forward vector and camera up vector (cross(forward, camera up direction))
    camera up - camera's new up vector, vector orthogonal to left x-axis and forward vector (cross(left, forward))

    dot products in the translate column is shorthand for multiplying translation per rotation axis.
*/

void matrixLookAt(mat4 m, vec3 pos, vec3 at, vec3 up)
{
  vec3 forward, left, camera_up;

  matrixIdentity(m);

  vectorSub(forward, at, pos);
  vectorNormalize(forward);

  vectorCross(left, forward, up);
  vectorNormalize(left);

  vectorCross(camera_up, left, forward);
  vectorNormalize(camera_up);

  m[0] =  left[0];              m[4] =  left[1];         m[8]  =  left[2];              m[12] = -vectorDot(left, pos);
  m[1] =  camera_up[0];         m[5] =  camera_up[1];    m[9]  =  camera_up[2];         m[13] = -vectorDot(camera_up, pos);
  m[2] = -forward[0];           m[6] = -forward[1];      m[10] = -forward[2];           m[14] =  vectorDot(forward, pos);
  m[3] = 0;                     m[7] = 0;                m[11] = 0;                     // m[15] = 1;
}

#endif
