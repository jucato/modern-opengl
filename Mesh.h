#ifndef MESH_H
#define MESH_H

#include <GL/glew.h>

enum { VAO_BOX, VAO_AXIS, VAO_GRID, NUM_VAO };
enum { VERT_BUF, NORM_BUF, TEX_BUF, COL_BUF, AXIS_VERT_BUF, AXIS_COL_BUF, GRID_VERT_BUF, GRID_COL_BUF, GRID_IND_BUF, GRID_TEX_BUF, NUM_VBO };
enum { vPos, vCol, vNorm, vTex };

const GLfloat boxSize = 1.0f;
int numVertices = 36;

GLfloat vertices[][3] = {
  // front
  { -boxSize, -boxSize,  boxSize },             // front tri 1
  {  boxSize, -boxSize,  boxSize },
  {  boxSize,  boxSize,  boxSize },
  {  boxSize,  boxSize,  boxSize },             // front tri 2
  { -boxSize,  boxSize,  boxSize },
  { -boxSize, -boxSize,  boxSize },
  // back
  { -boxSize, -boxSize, -boxSize },             // back tri 1
  {  boxSize, -boxSize, -boxSize },
  {  boxSize,  boxSize, -boxSize },
  {  boxSize,  boxSize, -boxSize },             // back tri 2
  { -boxSize,  boxSize, -boxSize },
  { -boxSize, -boxSize, -boxSize },
  // left
  { -boxSize, -boxSize, -boxSize },             // left tri 1
  { -boxSize, -boxSize,  boxSize },
  { -boxSize,  boxSize,  boxSize },
  { -boxSize,  boxSize,  boxSize },             // left tri 2
  { -boxSize,  boxSize, -boxSize },
  { -boxSize, -boxSize, -boxSize },
  // right
  {  boxSize, -boxSize, -boxSize },             // right tri 1
  {  boxSize, -boxSize,  boxSize },
  {  boxSize,  boxSize,  boxSize },
  {  boxSize,  boxSize,  boxSize },             // right tri 2
  {  boxSize,  boxSize, -boxSize },
  {  boxSize, -boxSize, -boxSize },
  // top
  { -boxSize,  boxSize,  boxSize },             // top tri 1
  {  boxSize,  boxSize,  boxSize },
  {  boxSize,  boxSize, -boxSize },
  {  boxSize,  boxSize, -boxSize },             // top tri 2
  { -boxSize,  boxSize, -boxSize },
  { -boxSize,  boxSize,  boxSize },
  // bottom
  { -boxSize, -boxSize,  boxSize },             // bottom tri 1
  {  boxSize, -boxSize,  boxSize },
  {  boxSize, -boxSize, -boxSize },
  {  boxSize, -boxSize, -boxSize },             // bottom tri 2
  { -boxSize, -boxSize, -boxSize },
  { -boxSize, -boxSize,  boxSize }
};

GLfloat normals[][3] = {
  // front
  {  0.0f,  0.0f,  1.0f },             // front tri 1
  {  0.0f,  0.0f,  1.0f },
  {  0.0f,  0.0f,  1.0f },
  {  0.0f,  0.0f,  1.0f },             // front tri 2
  {  0.0f,  0.0f,  1.0f },
  {  0.0f,  0.0f,  1.0f },
  // back
  {  0.0f,  0.0f, -1.0f },             // back tri 1
  {  0.0f,  0.0f, -1.0f },
  {  0.0f,  0.0f, -1.0f },
  {  0.0f,  0.0f, -1.0f },             // backtri 2
  {  0.0f,  0.0f, -1.0f },
  {  0.0f,  0.0f, -1.0f },
  // left
  { -1.0f,  0.0f,  0.0f },             // left tri 1
  { -1.0f,  0.0f,  0.0f },
  { -1.0f,  0.0f,  0.0f },
  { -1.0f,  0.0f,  0.0f },             // left tri 2
  { -1.0f,  0.0f,  0.0f },
  { -1.0f,  0.0f,  0.0f },
  // right
  {  1.0f,  0.0f,  0.0f },             // right tri 1
  {  1.0f,  0.0f,  0.0f },
  {  1.0f,  0.0f,  0.0f },
  {  1.0f,  0.0f,  0.0f },             // right tri 2
  {  1.0f,  0.0f,  0.0f },
  {  1.0f,  0.0f,  0.0f },
  // top
  {  0.0f,  1.0f,  0.0f },             // top tri 1
  {  0.0f,  1.0f,  0.0f },
  {  0.0f,  1.0f,  0.0f },
  {  0.0f,  1.0f,  0.0f },             // top tri 2
  {  0.0f,  1.0f,  0.0f },
  {  0.0f,  1.0f,  0.0f },
  // bottom
  {  0.0f, -1.0f,  0.0f },             // bottom tri 1
  {  0.0f, -1.0f,  0.0f },
  {  0.0f, -1.0f,  0.0f },
  {  0.0f, -1.0f,  0.0f },             // bottom tri 2
  {  0.0f, -1.0f,  0.0f },
  {  0.0f, -1.0f,  0.0f }
};

GLfloat texcoords[][2] = {
  // front
  { 0.0f, 0.0f },
  { 1.0f, 0.0f },
  { 1.0f, 1.0f },
  { 0.0f, 1.0f },
  // back
  { 0.0f, 0.0f },
  { 1.0f, 0.0f },
  { 1.0f, 1.0f },
  { 0.0f, 1.0f },
  // left
  { 0.0f, 0.0f },
  { 1.0f, 0.0f },
  { 1.0f, 1.0f },
  { 0.0f, 1.0f },
  // right
  { 0.0f, 0.0f },
  { 1.0f, 0.0f },
  { 1.0f, 1.0f },
  { 0.0f, 1.0f },
  // top
  { 0.0f, 0.0f },
  { 1.0f, 0.0f },
  { 1.0f, 1.0f },
  { 0.0f, 1.0f },
  // bottom
  { 0.0f, 0.0f },
  { 1.0f, 0.0f },
  { 1.0f, 1.0f },
  { 0.0f, 1.0f }
};

GLfloat colors[][3] = {
  // front
  {  0.0f,   0.0f,   1.0f },
  {  0.0f,   0.0f,   1.0f },
  {  0.0f,   0.0f,   1.0f },
  {  0.0f,   0.0f,   1.0f },
  {  0.0f,   0.0f,   1.0f },
  {  0.0f,   0.0f,   1.0f },
  // back
  {  1.0f,   0.0f,   1.0f },
  {  1.0f,   0.0f,   1.0f },
  {  1.0f,   0.0f,   1.0f },
  {  1.0f,   0.0f,   1.0f },
  {  1.0f,   0.0f,   1.0f },
  {  1.0f,   0.0f,   1.0f },
  // left
  {  0.0f,   1.0f,   0.0f },
  {  0.0f,   1.0f,   0.0f },
  {  0.0f,   1.0f,   0.0f },
  {  0.0f,   1.0f,   0.0f },
  {  0.0f,   1.0f,   0.0f },
  {  0.0f,   1.0f,   0.0f },
  // right
  {  1.0f,   1.0f,   0.0f },
  {  1.0f,   1.0f,   0.0f },
  {  1.0f,   1.0f,   0.0f },
  {  1.0f,   1.0f,   0.0f },
  {  1.0f,   1.0f,   0.0f },
  {  1.0f,   1.0f,   0.0f },
  // top
  {  1.0f,   0.0f,   0.0f },
  {  1.0f,   0.0f,   0.0f },
  {  1.0f,   0.0f,   0.0f },
  {  1.0f,   0.0f,   0.0f },
  {  1.0f,   0.0f,   0.0f },
  {  1.0f,   0.0f,   0.0f },
  // bottom
  {  0.0f,   1.0f,   1.0f },
  {  0.0f,   1.0f,   1.0f },
  {  0.0f,   1.0f,   1.0f },
  {  0.0f,   1.0f,   1.0f },
  {  0.0f,   1.0f,   1.0f },
  {  0.0f,   1.0f,   1.0f }
};

GLfloat axes_verts[][3] = {
  // +x
  { 0.0f, 0.0f, 0.0f},
  { 100.0f, 0.0f, 0.0f},
  // -x
  { 0.0f, 0.0f, 0.0f},
  { -100.0f, 0.0f, 0.0f},
  // +y
  { 0.0f, 0.0f, 0.0f},
  { 0.0f, 100.0f, 0.0f},
  // -y
  { 0.0f, 0.0f, 0.0f},
  { 0.0f, -100.0f, 0.0f},
  // +z
  { 0.0f, 0.0f, 0.0f},
  { 0.0f, 0.0f, 100.0f},
  // -z
  { 0.0f, 0.0f, 0.0f},
  { 0.0f, 0.0f, -100.0f},
};

GLfloat axes_col[][3] = {
  // +x
  { 1.0f, 0.0f, 0.0f},
  { 1.0f, 0.0f, 0.0f},
  // -x
  { 0.5f, 0.0f, 0.0f},
  { 0.5f, 0.0f, 0.0f},
  // +y
  { 0.0f, 1.0f, 0.0f},
  { 0.0f, 1.0f, 0.0f},
  // -y
  { 0.0f, 0.5f, 0.0f},
  { 0.0f, 0.5f, 0.0f},
  // +z
  { 0.0f, 0.0f, 1.0f},
  { 0.0f, 0.0f, 1.0f},
  // -z
  { 0.0f, 0.0f, 0.5f},
  { 0.0f, 0.0f, 0.5f},
};

const GLfloat gridSize = 5.0f;

GLfloat grid_verts[][3] = {
  {  gridSize, 0.0f,  gridSize },
  {  gridSize, 0.0f, -gridSize },
  { -gridSize, 0.0f, -gridSize },
  { -gridSize, 0.0f, -gridSize },
  { -gridSize, 0.0f,  gridSize },
  {  gridSize, 0.0f,  gridSize }
};

GLfloat grid_col[][3] = {
  { 1.0f, 1.0f, 1.0f },
  { 1.0f, 1.0f, 1.0f },
  { 1.0f, 1.0f, 1.0f },
  { 1.0f, 1.0f, 1.0f },
  { 1.0f, 1.0f, 1.0f },
  { 1.0f, 1.0f, 1.0f }
};

GLfloat grid_texCoords[][2] = {
  { 1.0f, 0.0f },
  { 1.0f, 1.0f },
  { 0.0f, 1.0f },
  { 0.0f, 1.0f },
  { 0.0f, 0.0f },
  { 1.0f, 0.0f }
};

#define BUFFER_OFFSET(bytes) ((GLubyte*) NULL + (bytes))

GLuint buffers[NUM_VBO];
GLuint vertexArrayObjects[NUM_VAO];

void initializeVertices(void)
{
  glGenVertexArrays(NUM_VAO, vertexArrayObjects);
  glBindVertexArray(vertexArrayObjects[VAO_BOX]);

  glGenBuffers(NUM_VBO, buffers);

  glBindBuffer(GL_ARRAY_BUFFER, buffers[VERT_BUF]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glVertexAttribPointer(vPos, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
  glEnableVertexAttribArray(vPos);

//   glBindBuffer(GL_ARRAY_BUFFER, buffers[COL_BUF]);
//   glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);
//   glVertexAttribPointer(vCol, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
//   glEnableVertexAttribArray(vCol);

  glBindBuffer(GL_ARRAY_BUFFER, buffers[NORM_BUF]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(normals), normals, GL_STATIC_DRAW);
  glVertexAttribPointer(vNorm, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
  glEnableVertexAttribArray(vNorm);

  glBindBuffer(GL_ARRAY_BUFFER, buffers[TEX_BUF]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(texcoords), texcoords, GL_STATIC_DRAW);
  glVertexAttribPointer(vTex, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
  glEnableVertexAttribArray(vTex);

//   create Axis

  glBindVertexArray(vertexArrayObjects[VAO_AXIS]);

  glBindBuffer(GL_ARRAY_BUFFER, buffers[AXIS_VERT_BUF]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(axes_verts), axes_verts, GL_STATIC_DRAW);
  glVertexAttribPointer(vPos, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
  glEnableVertexAttribArray(vPos);

  glBindBuffer(GL_ARRAY_BUFFER, buffers[AXIS_COL_BUF]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(axes_col), axes_col, GL_STATIC_DRAW);
  glVertexAttribPointer(vCol, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
  glEnableVertexAttribArray(vCol);

  glBindVertexArray(vertexArrayObjects[VAO_GRID]);

//   Create Grid

  glBindBuffer(GL_ARRAY_BUFFER, buffers[GRID_VERT_BUF]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(grid_verts), grid_verts, GL_STATIC_DRAW);
  glVertexAttribPointer(vPos, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
  glEnableVertexAttribArray(vPos);

  glBindBuffer(GL_ARRAY_BUFFER, buffers[GRID_COL_BUF]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(grid_col), grid_col, GL_STATIC_DRAW);
  glVertexAttribPointer(vCol, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
  glEnableVertexAttribArray(vCol);

  glBindBuffer(GL_ARRAY_BUFFER, buffers[GRID_TEX_BUF]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(grid_texCoords), grid_texCoords, GL_STATIC_DRAW);
  glVertexAttribPointer(vTex, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
  glEnableVertexAttribArray(vTex);

  glBindVertexArray(0);
}

void drawVertices(void)
{
  glBindVertexArray(vertexArrayObjects[VAO_BOX]);

  glDrawArrays(GL_TRIANGLES, 0, numVertices);

  glBindVertexArray(0);
}

void drawAxes(void)
{
  GLint numVertices;

  glBindVertexArray(vertexArrayObjects[VAO_AXIS]);

  glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &numVertices);
  glDrawArrays(GL_LINES, 0, numVertices);

  glBindVertexArray(0);
}

void drawGrid(void)
{
  GLint numVertices;

  glBindVertexArray(vertexArrayObjects[VAO_GRID]);

  glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &numVertices);
  glDrawArrays(GL_TRIANGLES, 0, numVertices);

  glBindVertexArray(0);
}

void destroyVertices(void)
{
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glDeleteBuffers(NUM_VBO, buffers);

  glBindVertexArray(0);
  glDeleteVertexArrays(NUM_VAO, vertexArrayObjects);
}


#endif